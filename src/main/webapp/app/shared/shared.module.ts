import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CatalogoprodutoSharedLibsModule, CatalogoprodutoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CatalogoprodutoSharedLibsModule, CatalogoprodutoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [CatalogoprodutoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CatalogoprodutoSharedModule {
  static forRoot() {
    return {
      ngModule: CatalogoprodutoSharedModule
    };
  }
}
