/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.jhonathancamacho.catalogoproduto.web.rest.vm;
